// This sample shows how to render a disabled RaisedButton, an enabled RaisedButton
// and lastly a RaisedButton with gradient background.

import 'package:flutter/material.dart';
import 'package:flutter_demo_by_thecodingpapa/src/buttons.dart';
import 'package:flutter_demo_by_thecodingpapa/src/form.dart';

void main() => runApp(MyApp());

var pageList = ['buttons', 'form', ];

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.RaisedButton',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyStatelessWidget(),
      routes: {
        '/${pageList[0]}': (context) => Buttons(),
        '/${pageList[1]}': (context) => MyForm(),
      },
    );
  }
}

class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Examples'
        ),
      ),
        body: ListView.separated(
      itemCount: pageList.length,
          separatorBuilder: (context, index) => Divider(
            color: Colors.transparent,
            height: 4,
          ),
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: InkWell(
            onTap: () => Navigator.pushNamed(context, '/${pageList[index]}'),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(pageList[index]),
              ),
            ),
          ),
        );
      },
    ));
  }
}
