import 'package:flutter/material.dart';



class MyForm extends StatefulWidget {
  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Form'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              validator: (value){
                if(value.isEmpty){
                  return 'Please enter anything';
                }
              },
            ),
            TextFormField(
              validator: (value){
                if(value.isEmpty){
                  return 'Please enter anything here too';
                }
              },
            ),
            RaisedButton(
              child: Text('Button'),
              onPressed: (){
                if(_formKey.currentState.validate()){
                  Scaffold.of(context).showSnackBar(SnackBar(content: Text('Wow form done!!')));
                }
              },
            )
          ],
        ),

      ),
    );
  }
}